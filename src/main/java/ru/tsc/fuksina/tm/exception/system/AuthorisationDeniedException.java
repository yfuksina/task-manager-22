package ru.tsc.fuksina.tm.exception.system;

import ru.tsc.fuksina.tm.exception.AbstractException;

public class AuthorisationDeniedException extends AbstractException {

    public AuthorisationDeniedException() {
        super("Error! Login or password is incorrect...");
    }

}
