package ru.tsc.fuksina.tm.exception.system;

import ru.tsc.fuksina.tm.exception.AbstractException;

public class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(String command) {
        super("Error! Command `" + command + "` not supported...");
    }

}
