package ru.tsc.fuksina.tm.api.service;

import ru.tsc.fuksina.tm.api.repository.IUserOwnedRepository;
import ru.tsc.fuksina.tm.enumerated.Sort;
import ru.tsc.fuksina.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

}
