package ru.tsc.fuksina.tm.api.repository;

import ru.tsc.fuksina.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findOneByLogin(String login);

    User findOneByEmail(String email);

    User removeByLogin(String login);

}
